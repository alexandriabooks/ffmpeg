import logging
import typing

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}

try:
    from .transcoder import FFmpegAudioTranscoderService, find_ffmpeg

    if find_ffmpeg():
        ALEXANDRIA_PLUGINS["AudioTranscoderService"] = [FFmpegAudioTranscoderService]
        ALEXANDRIA_PLUGINS["BookTranscoderService"] = [FFmpegAudioTranscoderService]
    else:
        logger.debug("Cannot find FFmpeg binary, not loading FFmpeg-based transcoders.")
except ImportError as e:
    logger.debug("Not loading ffmpeg plugin", exc_info=e)
