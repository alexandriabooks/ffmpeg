import logging
import typing
import os
import os.path
import tempfile
import asyncio

from alexandriabooks_core import model, utils
from alexandriabooks_core.services import (
    AudioTranscoderService,
    BookTranscoderService
)

logger = logging.getLogger(__name__)

SUPPORTED_AUDIO_TYPES = [
    model.AudioType.FLAC,
    model.AudioType.M4A,
    model.AudioType.MP3,
    model.AudioType.OGG,
    model.AudioType.WAV,
    model.AudioType.WMA
]

SUPPORTED_SOURCE_TYPES = [
    model.SourceType.FLAC,
    model.SourceType.M4A,
    model.SourceType.MP3,
    model.SourceType.OGG,
    model.SourceType.WAV,
    model.SourceType.WMA
]


def find_ffmpeg():
    for path in os.environ["PATH"].split(":"):
        filename = os.path.join(path, "ffmpeg")
        if os.path.isfile(filename):
            return filename
    logger.debug("Could not find FFmpeg...")
    return None


class FFmpegAudioTranscoderService(AudioTranscoderService, BookTranscoderService):
    def __init__(self, **kwargs):
        self._ffmpeg_path = find_ffmpeg()

    def __str__(self):
        return f"FFmpeg Audio Transcoder: {self._ffmpeg_path}"

    async def get_audio_transcode_targets(
        self, src: model.AudioType
    ) -> typing.List[model.AudioType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        if src in SUPPORTED_AUDIO_TYPES:
            return SUPPORTED_AUDIO_TYPES
        return []

    async def transcode_audio(
        self,
        src: typing.IO[bytes],
        desired_type: model.AudioType,
        out_stream: typing.Optional[typing.IO[bytes]] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes audio data.
        :param src: The audio source.
        :param desired_type: The type to transcode to.
        :param out_stream: Stream to write to, if None, then a stream is returned.
        :return: The transcoded image, or None if the image cannot be transcoded.
        """
        with tempfile.NamedTemporaryFile("wb") as src_file:
            await utils.copy_streams(src, src_file)
            src_file.flush()

            dest_file = tempfile.NamedTemporaryFile(
                "rb",
                prefix="ab_transcode",
                suffix="." + desired_type.ext()
            )
            dest_file.__enter__()

            try:
                process = await asyncio.create_subprocess_exec(
                    self._ffmpeg_path,
                    "-y",
                    "-i",
                    src_file.name,
                    dest_file.name,
                    stdout=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE
                )

                stdout, stderr = await process.communicate()
                if process.returncode != 0:
                    logger.warning("FFMPEG transcoding failed, stdout:\n%s", stdout)
                    logger.warning("FFMPEG transcoding failed, stderr:\n%s", stderr)
                    raise Exception("FFMPEG could not transcode audio file")

                if out_stream is not None:
                    await utils.copy_streams(dest_file, out_stream)
                    dest_file.__exit__(None, None, None)
                    return out_stream
                else:
                    return dest_file
            except Exception:
                dest_file.__exit__(None, None, None)
                raise

    async def get_book_transcode_targets(
        self, src: model.SourceType
    ) -> typing.List[model.SourceType]:
        """
        Returns a list of types the source can be transcoded into.
        :param src: The source.
        :return: The list of types that the source can be transcoded into.
        """
        if src in SUPPORTED_SOURCE_TYPES:
            return SUPPORTED_SOURCE_TYPES
        return []

    async def transcode_book(
        self,
        book_source: model.SourceProvidedBookMetadata,
        accessor: model.FileAccessor,
        desired_type: model.SourceType,
        image_transcoder_options: typing.Dict[str, typing.Any] = None,
        audio_transcoder_options: typing.Dict[str, typing.Any] = None,
        **kwargs
    ) -> typing.Optional[typing.IO[bytes]]:
        """
        Transcodes a book.
        :param book_source: The book to transcode.
        :param accessor: The book's accessor.
        :param desired_type: The type to transcode to.
        :param image_transcoder_options: Options to pass to the image transcoder,
          if needed.
        :param audio_transcoder_options: Options to pass to the image transcoder,
          if needed.
        :return: The transcoded book, or None if the book cannot be transcoded.
        """
        if desired_type not in SUPPORTED_SOURCE_TYPES:
            return None
        desired_audio_type = model.AudioType.to_audio_type(desired_type.name)
        if audio_transcoder_options is None:
            audio_transcoder_options = {}
        else:
            audio_transcoder_options = audio_transcoder_options.copy()
        audio_transcoder_options["desired_type"] = desired_audio_type
        return await self.transcode_audio(
            await accessor.get_stream(),
            **audio_transcoder_options
        )
